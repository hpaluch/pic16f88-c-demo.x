/*
 * uart.c - UART handling code for PIC16F88
 *
 * Functionally it is equivalent of assembler version from:
 * https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/uart.asm?at=master&fileviewer=file-view-default
 *
 * HW: PIC16F88
 * SW: MPLAB X IDE v2.35
 *     MPLAB XC8   v1.45
 */

#include <xc.h>

// initialize UART for transmit and receive on 9600 Bd
void uart_init(void){
    // init TX part of UART, 9600Bd, 8-bit, 1-start, 1-stop, no parity
    // see DS30487D-page 103

    // 1. set SPBRG (baud rate)
    SPBRG = 25; // 9600 Bd
    // 2.set TXSTA - SYNC must be cleared, BRGH=1 for better precision
    TXSTA = _TXSTA_BRGH_MASK;
    // set TRISB as input - see DS30487D-page 97
    TRISBbits.TRISB2 = 1;
    TRISBbits.TRISB5 = 1;
    // 3. must enable SPEN
    RCSTA = _RCSTA_SPEN_MASK;
    // 4 enable transmit - TXEN=1
    TXSTAbits.TXEN = 1;

    // init RX part of UART, 9600Bd, 8-bit, 1-start, 1-stop, no parity
    // see DS30487D-page 105
    // 1. set Baud Rate in SPBRG - already done in TX part
    // 2. clear SYNC - done
    // 3. enable SPEN - done
    // 4. clear RX9D - done
    // 5. enable RX reception - setting CREN
    RCSTAbits.CREN = 1;
}

// send character in 'c' via UART from PIC to PC
void uart_tx(unsigned char c){
    // wait until UART is ready for TX
    while( PIR1bits.TXIF == 0){
        NOP();
    }
    TXREG = c;
}

unsigned char uart_rx(void){
    while(1){
        // wait until data or error are available
        while(PIR1bits.RCIF ==0){
            NOP();
        }

        if (RCSTA & (_RCSTA_OERR_MASK|_RCSTA_FERR_MASK)){
            // error occured - transmit it to PC
            uart_tx('!');
            // 'O' for overrun error, 'F' for framing error
            uart_tx( RCSTA & _RCSTA_OERR_MASK ? 'O' : 'F'  );
            // required recovery after error - disable and enable CREN
            RCSTAbits.CREN=0;
            NOP();
            RCSTAbits.CREN=1;
            NOP();
        } else {
            // OK just return received char
            return RCREG;
        }
    }
}