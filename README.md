# PIC16F88 C version

This is functionally equivalent version of
demo written in ASM as you can see [main.asm]

Try to compare:

* [main.asm] vs. [main.c] 
* [uart.asm] vs. [uart.c]

Project status (from [ASM README]):

1. Outputs FOSC/4 on pin 15 RA6/OSC2/CLKO.
1. output 7.6/4 on RA0 PIN 17 - connected to Speaker
1. LEDs on RA1 controlled via Serial commands `0` or `1`

![PIC16F88 demo schematic](https://bytebucket.org/hpaluch/pic16f88-demo.x/raw/ba8c39db593e425d65f3cd096aa00789d9040479/ExpressPCB/pic16f88-demo.png)

Supported commands on serial port:

* `0` - sends logical 0 to RA1 - red LED D1 is on, green LED D2 is off
* `1` - sends logical 1 to RA1 - red LED D1 is off, green LED D2 is on

Answers from PIC:

* `=0` or `=1` - command processed successfully - the number is value
  from command (`0` or `1`)
* `!C` - invalid command - entered anything other than `0` or `1`
* `!F` - framing error - invalid data received from serial port
* `!O` - overrun error - some data lost from serial port

Please see [ASM README] for detailed info on project.


# Hardware requirements

* [DM163045 - PICDEM Lab Development Kit][DM163045], this kit includes also
  PicKit 3 Programmer/Debugger and PIC MCUs samples
* [PIC16F88][PIC16F88] - included with [DM163045 PICDEM kit][DM163045].
  Please see [PIC16F88 Data Sheet] for more information on this MCU
* [USB Console Cable #954] to transfer serial data from PIC to PC
  
# Software requirements
  
* [MPLAB X IDE v2.35] - the oldest version that checks calibration value
  and allow overwrite of calibration and still fast and works under XP
  (unlike current version 4.x)
* [MPLAB XC8 v1.45] - last version of C compiler for 8-bit MCUs, Free
  edition


[ASM README]: https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/README.md?fileviewer=file-view-default "PIC16F88 ASM demo README"
[main.asm]: https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/main.asm?fileviewer=file-view-default "PIC16F88 demo main.asm"
[uart.asm]: https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/uart.asm?at=master&fileviewer=file-view-default "PIC16F88 demo uart.asm"
[main.c]: https://bitbucket.org/hpaluch/pic16f88-c-demo.x/src/e8795a6abb54bb311bd542409c83ab9dc5b37563/main.c?at=master&fileviewer=file-view-default "PIC16F88 demo main.c"
[uart.c]: https://bitbucket.org/hpaluch/pic16f88-c-demo.x/src/e8795a6abb54bb311bd542409c83ab9dc5b37563/uart.c?at=master&fileviewer=file-view-default "PIC16F88 demo uart.c"
[MPLAB X IDE v2.35]: http://www.microchip.com/development-tools/pic-and-dspic-downloads-archive "MPLAB Archive Download"
[DM163045]: http://www.microchip.com/Developmenttools/ProductDetails/DM163045 "PICDEM Lab Development Kit"
[PIC16F88]: https://www.microchip.com/wwwproducts/en/PIC16F88 "PIC16F88 Overview"
[PIC16F88 Data Sheet]: http://ww1.microchip.com/downloads/en/DeviceDoc/30487D.pdf "PIC16F88 Data Sheet"
[MPLAB XC8 v1.45]: http://ww1.microchip.com/downloads/en/DeviceDoc/xc8-v1.45-full-install-windows-installer.exe "MPLAB XC8 v1.45 Windows Download" 
[USB Console Cable #954]: https://www.modmypi.com/raspberry-pi/communication-1068/serial-1075/usb-to-ttl-serial-cable-debug--console-cable-for-raspberry-pi "USB Console Cable #954" 