/* 
 * main.c - PIC16F88 demo written in XC8
 *
 * This is complementary project to assembler version
 * from: https://bitbucket.org/hpaluch/pic16f88-demo.x
 *
 * This version of C code should be equivalent of:
 * ASM code from: https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/main.asm?at=master&fileviewer=file-view-default
 *
 * Please see
 * https://bitbucket.org/hpaluch/pic16f88-demo.x/src/ba8c39db593e425d65f3cd096aa00789d9040479/README.md?fileviewer=file-view-default
 * for functional description.
 */

// SW: MPLAB X IDE v2.35 (runs smoothly on XP!)
//     MPLAB XC8   v1.45 C compiler Free version

#include <xc.h>

#include "uart.h"

// CONFIG1
#pragma config FOSC = INTOSCCLK // Oscillator Selection bits (INTRC oscillator; CLKO function on RA6/OSC2/CLKO pin and port I/O function on RA7/OSC1/CLKI pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#ifdef __DEBUG
#pragma config PWRTE = OFF       // Power-up Timer Enable bit (PWRT disabled fo debug)
#else
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#endif
#pragma config MCLRE = ON       // RA5/MCLR/VPP Pin Function Select bit (RA5/MCLR/VPP pin function is MCLR)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF        // Low-Voltage Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection bit (Code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off)
#pragma config CCPMX = RB0      // CCP1 Pin Selection bit (CCP1 function on RB0)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

// CONFIG2
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
#pragma config IESO = OFF       // Internal External Switchover bit (Internal External Switchover mode disabled)

// My I/O ports
#define iSPKR_MASK _PORTA_RA0_MASK
#define iLED_MASK  _PORTA_RA1_MASK

// show to user which mode is compiled
#ifdef __DEBUG
#warning Build in Debug mode
#else
#warning Build in Production (Run) mode
#endif

unsigned char counter;

// ISR from DS50002173A-page 27
void interrupt myIsr(void){
    if(INTCONbits.TMR0IE && INTCONbits.TMR0IF) {
         INTCONbits.TMR0IF = 0; // must acknowledge Timer0 interrupt
         counter ++;
         // copy BIT 0 of counter to RA0 (speaker)
         PORTAbits.RA0 = (counter & 1)!=0 ? 1 : 0;
    }
}

int main(void) {
    counter = 0;    // set counter to known state

    OSCCONbits.IRCF = 0b110;    // 1 MHz instruction clock
    PORTA = 0;                  // set latches on PORTA to known value
    ANSEL = 0;                  //
    TRISA = ~( iSPKR_MASK | iLED_MASK); // output our I/O ports

    TMR0 = 0;   // defined state for TMR0
    // setup OPTION_REG
    CLRWDT(); // even when disabled WDT - see DS30487D-page 69
    OPTION_REG = _OPTION_REG_nRBPU_MASK|0x7;

    uart_init();

    INTCONbits.TMR0IE = 1;  // enable interrupts for timer 0
    ei();                   // enable all interrupts

    while(1){
        unsigned char c = uart_rx();

        if (c>= '0' && c<='1'){
            // valid command: output to LED
            PORTAbits.RA1 = (c=='1') ? 1 : 0;
            // send OK response
            uart_tx('='); uart_tx(c);
        } else {
            // unknown 'C'-ommand
            uart_tx('!');uart_tx('C');
        }
    }

    return 0;
}

