/* 
 * File:   uart.h - UART (RS232) functions
 * Created on July 15, 2018, 6:08 PM
 */

#ifndef UART_H
#define	UART_H

extern void uart_init(void);
extern void uart_tx(unsigned char c);
extern unsigned char uart_rx(void);

#endif	/* UART_H */

